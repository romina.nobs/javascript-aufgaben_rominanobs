// Aufgabe 1)
// Implementieren Sie eine JavaScript Funktion die einen String umkehrt.
// Beispiel: aus "javascript" wird "tpircsavaj"

const reverseString = (str) => {
  return str.split("").reverse().join("");
};

console.log(reverseString("javascript"));

//   Aufgabe 2)
//   Implementieren Sie eine JavaScript Funktion, welche erkennt, ob ein Jahr ein Schaltjahr ist. Sie erkennen ein Schaltjahr anhand der folgenden Regeln:
//   -    Die durch 4 ganzzahlig teilbaren Jahre sind Schaltjahre.
//   -    Jahre, die ein Jahrhundert abschliessen (z. B. 1800, 1900, 2100 und 2200) sind keine Schaltjahre.
//   -    Die durch 400 ganzzahlig teilbaren Jahre sind doch Schaltjahre.
//   Bspw. ist 1997 kein Schaltjahr, aber 1996 schon. 1900 ist kein Schaltjahr, 2000 ist eines.

// Prüfen, ob ein Schaltjahr vorliegt/wiki/JavaScript/Tutorials/Zeitberechnung#Pr.C3.BCfen.2C_ob_ein_Schaltjahr_vorliegt

// Anhand der Schaltregeln des gregorianischen Kalenders können wir überprüfen, ob ein Jahr ein Schaltjahr ist.

//     Ein Jahr ist ein Schaltjahr, wenn seine Jahreszahl durch 4 teilbar ist
//     aber es ist kein Schaltjahr, wenn seine Jahreszahl durch 100 teilbar ist
//     aber es ist doch ein Schaltjahr, wenn seine Jahreszahl durch 400 teilbar ist

// Das lässt sich zusammenfassen zu

//     Ein Jahr ist ein Schaltjahr, wenn seine Jahreszahl durch 4 aber nicht durch 100 teilbar ist.
//     Ein Jahr ist ein Schaltjahr, wenn seine Jahreszahl durch 400 teilbar ist.

function isLeapYear(year) {
  return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}
console.log(isLeapYear(2020)); // true;

// Aufgabe 3)

// Schreiben Sie eine JavaScript Funktion, die erkennt, ob ein Satz ein Pangramm ist. Ein Pangramm ist ein Satz, welcher jeden Buchstaben des Alphabets mind. einmal benutzt. Das bekannteste Pangramm im Englischen ist:
//     The quick brown fox jumps over the lazy dog.
// Das Alphabet benutzt nur die ASCII-Zeichen a-z. Gross-/Kleinschreibung muss nicht unterschieden werden.

function pangrams(s) {
  let alphabet = "abcdefghijklmnopqrstuvwxyz";
  let regex = /\s/g;
  let lowercase = s.toLowerCase().replace(regex, "");

  for (let i = 0; i < alphabet.length; i++) {
    if (lowercase.indexOf(alphabet[i]) === -1) {
      return "not pangram";
    }
  }

  return "pangram";
}

console.log(pangrams("The quick brown fox jumps over the lazy dog."));
